# Simili-lovebox

<img style="float: right" align="right" src="https://gitlab.com/pastel-fablab/orga/raw/master/logo/fablab-logo-transparent-300dpi.png" width="350">

Le principe de ce projet est de reprendre le principe d'une love box commercialisée 100€ pour beaucoup moins cher... Et faite main !
https://fr.lovebox.love/

![photo_montage](./images/proto_montage.jpg "photo_montage")

## Fiche projet 
| **Caractéristiques du Projet**  |   |
| --- | --- |
| **Technologies** | ESP8266, Leds, WebServer, Web API, Impression 3D
| **Coût** | 10€
| **Temps de réalisation** | 5h.
| **Difficulté** | Moyenne.
| **Equipe réalisatrice** | Pierre Pochon
| **Documentation** | https://gitlab.com/pastel-fablab/projets/simili-lovebox

## Matériel

- ESP 8266 (Wemos D1 Mini)
- LED rouge
- résistance 100 ohms
- Ecran OLED ssd1306
- Platine a essai et cables

## Description détaillée 

Le but de ce projet est de reprendre le principe de la [lovebox](https://fr.lovebox.love/).
Pour rappel, l'objet de base est commercialisé 100€ et permet d'afficher un message personnalisé via une application. Lorsqu'un nouveau message est défini, un petit coeur tourne pour signifier qu'un nouveau message est disponible.

Dans notre cas, nous allons implémenter les modifications de cas d'utilisateur suivants :
- le coeur rotatif est remplacé par une led qui clignote au rythme d'un battement de coeur. Un servomoteur étant trop gros à intégrer par rapport à l'ESP et l'écran OLED ssd1306, il a été décider de jouer la carte miniature, plutôt que de rester collé à l'idée de base. 
- la love box ira intérroger tous les jours une API Web pour récupérer et afficher une citation d'amour <3. L'utilisateur aura toujours la possibilité d'afficher des messages personnalisés via un webserver généré par l'ESP.

- ESP 8266 : le micro-controlleur intégrant un puce Wifi qui gérera l'intelligence du système
- LED rouge : Pour illuminer un coeur, afn de signifier qu'un nouveau message est disponible.
- résistance 100 ohms : a monter en sériée avec la LED
- Ecran OLED ssd1306 : Pour afficher les messages. Fonctionne en BUS I2C (4 câbles)
- Platine a essai et cables : pour le montage.
- Impression 3D : le tout bien rangé dans une mini boite pour une présentation propre.

Quelques fonctionnalités : 
- lib MultiWifi : pour permettre de se connecter à plusieurs réseau Wifi (au cas o ù on bouge la lovebox)
- Récupération de nouvelle Citation depuis une API web toutes les 24h (déclenchement sur timer OS)
- L'ouverture / fermeture du capot est détecté par un contact. En fonction de contact, la lovebox acquittera le message.
- L'ESP8266 génèrera un serverWeb avec une interface, qui permettra à l'utilisateur d'ajouter lui même un message. Celui-ci écrasera le message existant.

### Câblage

![cablage](./images/lovebox_bb.png "Cablage")

### Impression 3d

![Case](./images/case.png "Case")

Réalisé sous Fusion360: Voir fichier [f3d](./lovebox v4.f3d)

### A améliorer

Toute contribution est la bienvenue :)
- [ ] Gestion du word-wrap dans les messages.
- [ ] Test intégration avec un servo moteur
- [ ] Version sans-fil avec une batterie Lipo et en jouant intelligemment avec le mode deepsleep de l'ESP

## Liens

- Objectif : https://fr.lovebox.love/
- Gestion WebServeur : https://randomnerdtutorials.com/esp32-esp8266-input-data-html-form/
- Timer : https://www.switchdoc.com/2015/10/iot-esp8266-timer-tutorial-arduino-ide/
- Client HTTPS : https://buger.dread.cz/simple-esp8266-https-client-without-verification-of-certificate-fingerprint.html


## License
Ce projet est sous licence Apache 2.0. Voire le fichier [LICENSE](LICENSE.md) pour plus de détails
