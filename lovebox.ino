#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

//////////////////////////
//     CONFIGURATION
//////////////////////////
// pin used for heartbeat
int PIN_HEARTBEAT = D7;
// pin used for make the box'door opened
int PIN_OPEN_BOX = D3;
//Wifi connection credentials
const char* WifiSSID = "MonWifi";
const char* WifiPWD = "monpassword";
//get a new quote every 24h
int TIMER_QUOTE = 24 * 3600 * 1000;
//////////////////////////

//WerServer
ESP8266WebServer server ( 80 );
//Wifi management
ESP8266WiFiMulti WiFiMulti;

//OLED
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


//variable containing the current message
String message = "";
//Bool if new message (not acknowledged)
bool new_message = false;
boolean last_opened_state = false;

//timer to get new citation
os_timer_t myTimer;
bool tickOccured;

// start of timerCallback
void timerGetCitation(void *pArg) {
  tickOccured = true;
}

// function to print message on the OLED screen
void printOLED(String p_message) {
  //first print in serial
  Serial.println(p_message);
  //then on the OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(p_message);
  display.display();
}

String getPage() {
  String page = "<!DOCTYPE HTML><html><head>";
  page += "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">";
  page += "  </head><body>";
  page += "  <form method=\"get\">";
  page += "    <h1>Pastel Fablab LoveBox</h1>";
  page += "    Message: <input type=\"text\" max-lenght:\"64\" name=\"message\" value=\"" + message + "\">";
  page += "    <input type=\"submit\" value=\"Submit\">";
  page += "  </form>";
  page += "</body></html>";
  return page;
}

String getCitation() {

  //if there already is a new message, skipping...
  if (new_message) {
    return "";
  }

  String citation;
  citation = "citation";

  std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

  client->setInsecure();

  HTTPClient https;

  Serial.print("[HTTPS] begin...\n");
  if (https.begin(*client, "https://citations.ouest-france.fr/apis/export.php?lite=1&key=464fzer5&t=theme&theme=amour")) {  // HTTPS

    Serial.print("[HTTPS] GET...\n");
    // start connection and send HTTP header
    int httpCode = https.GET();
    

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      citation="[HTTPS] GET... code: " + String(httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        citation = https.getString();
        Serial.println(citation);
      }
    } else {
      citation = "[HTTPS] GET... failed";
    }

    https.end();
  } else {
    citation = "[HTTPS] Unable to connect\n";
  }

  //fix citation:
  //remove accents and htlm tags.
  citation = citation.substring(5);
  citation.replace("<br />", "\n");
  citation.replace("é", "e");
  citation.replace("è", "e");
  citation.replace("ê", "e");
  citation.replace("à", "a");
  citation.replace("û", "u");
  citation.replace("ù", "u");
  citation.replace("ç", "c");
  citation.replace("ô", "o");

  //print on OLED
  printOLED(citation);
  new_message = true;

  return citation;
}



// handle main page
void handleRoot() {
  //set the message if given
  if ( server.hasArg("message") ) {
    message = server.arg("message");
    new_message = true;
    //write message on the OLED screen
    printOLED(message);
  }
  //response to the client
  server.send ( 200, "text/html", getPage() );
}

// Make an heartbeat
void heartbeat() {

  int tempo_between_2_beat = 150;
  int tempo_beat_high = 100;
  int tempo_between_2_heartbeat = 1000;

  //first beat
  digitalWrite(PIN_HEARTBEAT, LOW);
  delay(tempo_beat_high);
  digitalWrite(PIN_HEARTBEAT, HIGH);

  delay(tempo_between_2_beat);

  //second beat
  digitalWrite(PIN_HEARTBEAT, LOW);
  delay(tempo_beat_high);
  digitalWrite(PIN_HEARTBEAT, HIGH);

  //delay next heartbeat
  delay(tempo_between_2_heartbeat);
}

void setup() {
  //init serial for debuging
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println("Starting...");

  //init heartbeat led
  Serial.println("Setting led pin mode");
  pinMode(PIN_HEARTBEAT, OUTPUT);
  digitalWrite(PIN_HEARTBEAT, HIGH);

  //init opne/close mechanism
  //using pullup use internal resistance
  Serial.println("Setting open switch pin mode");
  pinMode(PIN_OPEN_BOX, INPUT_PULLUP);

  //Connect to Wifi
  Serial.println("Setting Wifi...");
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(WifiSSID, WifiPWD);

  //init the display
  Serial.println("Setting display...");
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  String msg = "Connecting to Wifi...";
  while (WiFiMulti.run() != WL_CONNECTED) {
    msg += ".";
    printOLED(msg);
    delay(500);
  }

  Serial.println("Setting HTTP server...");
  //link to the function that manage launch page
  server.on ( "/", handleRoot );
  server.begin();
  Serial.println ( "HTTP server started" );

  //Set a Welcome Message
  msg = "Pastel LoveBox <3\n\nVous pouvez envoyer des messages personnalises en vous connectant sur \nhttp://" + WiFi.localIP().toString();
  printOLED(msg);

  //wait 20s and get a new quote
  delay(5000);
  Serial.println("Getting citation...");
  //getCitation();

  //change citation every X seconds
  Serial.println("Setting timer...");
  os_timer_setfn(&myTimer, timerGetCitation, NULL);
  os_timer_arm(&myTimer, TIMER_QUOTE, true);
}

void loop() {

  if (WiFiMulti.run() == WL_CONNECTED) {

    //check if the box has been openned
    boolean is_box_opened = !digitalRead(PIN_OPEN_BOX);

    //if box is closed and got a new message
    if (!is_box_opened && new_message) {
      heartbeat();
    }

    if (is_box_opened && !last_opened_state)
    {
      Serial.println("box has been opened !");
      new_message = false;
    }
    //check if the box has been closed
    if (!is_box_opened && last_opened_state)
    {
      Serial.println("box has been closed !");
    }
    last_opened_state = is_box_opened;

    //Get new quote if the timer ticked
    if (tickOccured == true)
    {
      //getCitation();
      tickOccured = false;
    }

    //manage new requests
    server.handleClient();
  }

}
